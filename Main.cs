using System;
using System.Collections.Immutable;
using HRManagement;

class Program
{
    static void Main()
    {
        // Створення проєктів для використання
        var project1 = new Project
        {
            Id = 12,
            Name = "WebSite",
            Value = 2500
        };
        
        var project2 = new Project
        {
            Id = 27,
            Name = "MobileApp",
            Value = 3900
        };
        
        var project3 = new Project
        {
            Id = 89,
            Name = "DesktopApp",
            Value = 3300
        };
        
        var project4 = new Project
        {
            Id = 37,
            Name = "BusinessSoftware",
            Value = 5100
        };
        
        var project5 = new Project
        {
            Id = 55,
            Name = "AIIntegration",
            Value = 4800
        };
        
        // Створення екземплярів, що керують усіма процесами
        var employeeService = new EmployeeService();
        var departmentService = new DepartmentService();
        var positionService = new PositionService();
        var projectService = new ProjectService();

        var searchService = new SearchService(employeeService, projectService, positionService, departmentService);
        
        //Заповнення списку проєктами
        projectService.AddProject(project1);
        projectService.AddProject(project2);
        projectService.AddProject(project3);
        projectService.AddProject(project4);
        projectService.AddProject(project5);
        
        // Створення посад
        positionService.AddPosition(1, "Developer");
        positionService.AddPosition(23, "Engineer");
        positionService.AddPosition(18, "Manager");
        
        // Створення департаментів
        departmentService.AddDepartment(1, "Developers");
        departmentService.AddDepartment(23, "Engineers");
        departmentService.AddDepartment(18, "Managers");
        
        // Створення робітників
        employeeService.AddEmployee(1, "Mark", "Matviichuk", "UA1283 9082 0011", departmentService.AddEmployeeTo("Developers"), positionService.AssignEmployeeTo("Developer"), 5, 120, 1600);
        employeeService.AddEmployee(4, "Joe", "Biden", "US1123 8014 8801", departmentService.AddEmployeeTo("Developers"), positionService.AssignEmployeeTo("Developer"), 1, 140, 3700);
        employeeService.AddEmployee(2, "Bob", "Luki", "EU0012 9821 6381", departmentService.AddEmployeeTo("Engineers"), positionService.AssignEmployeeTo("Engineer"), 7, 105, 2400);
        employeeService.AddEmployee(7, "Poppy", "Sumsan", "GB9036 0204 1600", departmentService.AddEmployeeTo("Managers"), positionService.AssignEmployeeTo("Manager"), 1, 90, 800);
        employeeService.AddEmployee(3, "Lolly", "Pops", "AU2210 7634 1920", departmentService.AddEmployeeTo("Engineers"), positionService.AssignEmployeeTo("Engineer"), 4, 100, 1400);

        // Включення робітника в проєкт
        Console.WriteLine("-- Включення робітника в проєкт --");
        employeeService.AddEmployeeToProject(project2, 4);
        employeeService.AddEmployeeToProject(project4, 4);
        
        employeeService.AddEmployeeToProject(project2, 100);
        
        employeeService.AddEmployeeToProject(project1, 3);
        
        employeeService.AddEmployeeToProject(project3, 7);
        Console.WriteLine("-- . --");
        
        // Перевірка даних департаменту
        Console.WriteLine("-- Перевірка даних департаменту --");
        departmentService.DepartmentInfo(1);
        departmentService.DepartmentEmployees(1);
        
        departmentService.DepartmentInfo(23);
        departmentService.DepartmentEmployees(23);
        Console.WriteLine("-- . --");
        
        // Оновлення департаменту та перевірка оновлення робітників департаменту
        Console.WriteLine("-- Оновлення департаменту та перевірка оновлення робітників департаменту --");

        departmentService.UpdateDepartmentInfo(18, 77, "Admins");
        positionService.UpdatePositionInfo(18, 77, "Admin");
        
        employeeService.EmployeeInfo(7);
        Console.WriteLine("-- . --");

        // Виведення інформації про співробітника за ID
        Console.WriteLine("-- Виведення інформації про співробітника за ID --");
        employeeService.EmployeeInfo(1);
        employeeService.EmployeeInfo(5);
        employeeService.EmployeeInfo(7);
        Console.WriteLine("-- . --");
        
        // Виведення списку всіх співробітників
        Console.WriteLine("-- Виведення списку всіх співробітників --");
        Console.WriteLine(employeeService.GetEmployeesInfo());
        Console.WriteLine("-- . --");

        // Видалення співробітника за ID та відразу видалення його з департаменту
        Console.WriteLine("-- Видалення співробітника за ID та відразу видалення його з департаменту --");
        employeeService.RemoveEmployee(1);
        departmentService.DepartmentEmployees(1);
        Console.WriteLine("-- . --");
        
        // Виведення списку всіх співробітників після видалення
        Console.WriteLine("-- Виведення списку всіх співробітників після видалення --");
        Console.WriteLine(employeeService.GetEmployeesInfo());
        Console.WriteLine("-- . --");
        
        // Оновлення даних для робітника за ID
        Console.WriteLine("-- Оновлення даних для робітника за ID --");
        employeeService.UpdateEmployeeInfo(4, 9, "Chupa", "Pi", "CA0981 8920 9282", departmentService.AddEmployeeTo("Engineers"), positionService.AssignEmployeeTo("Engineer"), 1, 140, 130);
        employeeService.GetEmployeeProjects(9);
        Console.WriteLine("-- . --");
        
        // Виведення департаментів відсортованих різним чином
        Console.WriteLine("-- Виведення департаментів відсортованих різним чином --");
        departmentService.SortDepartmentEmployeesByPosition(23);
        departmentService.DepartmentEmployees(23);
        
        departmentService.SortDepartmentEmployeesByProjectValues(23);
        departmentService.DepartmentEmployees(23);
        Console.WriteLine("-- . --");
        
        // Виведення списку відсортованого по імені
        Console.WriteLine("-- Виведення списку відсортованого по імені --");
        employeeService.SortByFirstName();
        Console.WriteLine(employeeService.GetEmployeesInfo());
        Console.WriteLine("-- . --");
        
        // Виведення списку відсортованого по прізвищу
        Console.WriteLine("-- Виведення списку відсортованого по прізвищу --");
        employeeService.SortByLastName();
        Console.WriteLine(employeeService.GetEmployeesInfo());
        Console.WriteLine("-- . --");
        
        // Виведення списку відсортованого за зарплатнею
        Console.WriteLine("-- Виведення списку відсортованого за зарплатнею --");
        employeeService.SortBySalary();
        Console.WriteLine(employeeService.GetEmployeesInfo());
        Console.WriteLine("-- . --");
        
        // Виведення назв проєктів, в яких робітник брав участь
        Console.WriteLine("-- Виведення назв проєктів, в яких робітник брав участь --");
        employeeService.GetEmployeeProjects(9);
        employeeService.GetEmployeeProjects(7);
        Console.WriteLine("-- . --");
        
        // Виведення найпрестижніших посад
        Console.WriteLine("-- Виведення найпрестижніших посад --");
        positionService.Top5Positions();
        Console.WriteLine("-- . --");
        
        // Виведення напрофітніших робітників з кожної посади
        Console.WriteLine("-- Виведення напрофітніших робітників з кожної посади --");
        positionService.MostProfitableEmployee(1);
        positionService.MostProfitableEmployee(23);
        positionService.MostProfitableEmployee(77);
        Console.WriteLine("-- . --");
        
        //Пошук робітників по ключовому слову
        Console.WriteLine("-- Пошук спвіробітників по ключовому слову --");
        employeeService.SearchEmployeeByKeyword("Ch"); // Ім'я
        employeeService.SearchEmployeeByKeyword("ns"); // Назва департаменту
        employeeService.SearchEmployeeByKeyword("eu"); // Номер рахунку
        Console.WriteLine("-- . --");
        
        // Розширений пошук користувача
        Console.WriteLine("-- Розширений пошук користувача --");
        employeeService.AdvancedSearchEmployee("Admin Sumsan GB9036 0204 1600");
        Console.WriteLine("-- . --");

        // Пошук проєктів по ключовому слову
        Console.WriteLine("-- Пошук проєктів по ключовому слову --");
        projectService.SearchProjectByKeyword("App");
        Console.WriteLine("-- . --");
        
        // Пошук всього по ключовому слову
        Console.WriteLine("-- Пошук всього по ключовому слову --");
        searchService.SearchAllByKeyword("De");
        Console.WriteLine("-- . --");
    }
}