using System;
using System.Collections.Generic;
using System.Linq;

namespace HRManagement
{
    public class Employee
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AccountNumber { get; set; }
        public Department Department { get; set; }
        public Position Position { get; set; }
        public int WorkExperience { get; set; }
        
        public int WorkingHours { get; set; }
        public decimal Salary { get; set; }
        public List<Project> Projects { get; set; }

        public Employee(int id, string firstName, string lastName, string accountNumber, Department department, Position position, int workExperience, int workingHours, decimal salary)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            AccountNumber = accountNumber;
            Department = department;
            Position = position;
            WorkExperience = workExperience;
            WorkingHours = workingHours;
            Salary = salary;
            
            Projects = new List<Project>();
        }

        public string Info()
        {
            return $"ID: {Id}, Name: {FirstName}, Last Name: {LastName}, Account Number: {AccountNumber}, Department: {Department.Name}, Position: {Position.Name}, Work Experience: {WorkExperience}, Salary: {Salary}";
        }

        public void ChangeData(int id, string firstName, string lastName, string accountNumber, Department department, Position position, int workExperience, int workingHours, decimal salary)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            AccountNumber = accountNumber;
            Department = department;
            Position = position;
            WorkExperience = workExperience;
            WorkingHours = workingHours;
            Salary = salary;
        }
    }

    public class Department
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Employee> Employees { get; set; }

        public Department(int id, string name)
        {
            Id = id;
            Name = name;
            
            Employees = new List<Employee>();
        }
        
        public string Info()
        {
            return $"ID: {Id}, Name: {Name}";
        }
        
        public void ChangeData(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    public class Position
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public List<Employee> PositionEmployees { get; set; }
        
        public Position(int id, string name)
        {
            Id = id;
            Name = name;
            
            PositionEmployees = new List<Employee>();
        }
        
        public string Info()
        {
            return $"ID: {Id}, Name: {Name}";
        }
        
        public void ChangeData(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }

    public class Project
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Value { get; set; }
        public List<Employee> Employees { get; set; }

        public Project()
        {
            Employees = new List<Employee>();
        }
    }

    public class EmployeeService
    {
        public List<Employee> EmployeeList { get; set; }

        public EmployeeService()
        {
            EmployeeList = new List<Employee>();
        }

        public void AddEmployee(int id, string firstName, string lastName, string accountNumber, Department department, Position position, int workExperience, int workingHours, decimal salary)
        {
            Employee employee = new Employee(id, firstName, lastName, accountNumber, department, position, workExperience, workingHours, salary);

            department.Employees.Add(employee);
            position.PositionEmployees.Add(employee);
            
            EmployeeList.Add(employee);
        }

        public void RemoveEmployee(int employeeId)
        {
            Employee employee = EmployeeList.FirstOrDefault(e => e.Id == employeeId);
            
            if (employee != null)
            {
                EmployeeList.Remove(employee);
                employee.Department.Employees.Remove(employee);
                employee.Position.PositionEmployees.Remove(employee);
            }
            else
            {
                Console.WriteLine("Employee does not exist.");
            }
        }

        public void EmployeeInfo(int employeeId)
        {
            Employee employee = EmployeeList.FirstOrDefault(e => e.Id == employeeId);
            
            if (employee != null)
            {
                Console.WriteLine(employee.Info());
            }
            else
            {
                Console.WriteLine("Employee does not exist.");
            }
        }

        public string GetEmployeesInfo()
        {
            if (EmployeeList.Count == 0)
            {
                return "No employees in the list.";
            }

            List<string> employeeInfos = new List<string>();
            employeeInfos.Add("\nList of employees:");
            foreach (var employee in EmployeeList)
            {
                employeeInfos.Add(employee.Info());
            }

            return string.Join(Environment.NewLine, employeeInfos);
        }

        public void UpdateEmployeeInfo(int employeeId, int newId, string firstName, string lastName, string accountNumber, Department department, Position position, int workExperience, int workingHours, decimal salary)
        {
            Employee employee = EmployeeList.FirstOrDefault(e => e.Id == employeeId);

            if (employee != null)
            {
                employee.Department.Employees.Remove(employee);
                employee.Position.PositionEmployees.Remove(employee);
                
                employee.ChangeData(newId, firstName, lastName, accountNumber, department, position, workExperience, workingHours, salary);
                
                department.Employees.Add(employee);
                position.PositionEmployees.Add(employee);
            }
            else
            {
                Console.WriteLine("Employee does not exist.");
            }
        }
        
        public void AddEmployeeToProject(Project project, int employeeId)
        {
            Employee employee = EmployeeList.FirstOrDefault(e => e.Id == employeeId);

            if (employee != null)
            {
                employee.Projects.Add(project);
                project.Employees.Add(employee);
            }
            else
            {
                Console.WriteLine("Employee does not exist.");
            }
        }

        public void GetEmployeeProjects(int employeeId)
        {
            Employee employee = EmployeeList.FirstOrDefault(e => e.Id == employeeId);
            
            if (employee != null)
            {
                Console.WriteLine($"Projects for employee ID {employeeId}:");
                if (employee.Projects.Count == 0)
                {
                    Console.WriteLine("Employee has no projects.");
                }
                else
                {
                    foreach (var project in employee.Projects)
                    {
                        Console.Write($"{project.Name} ");
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Employee does not exist.");
            }
        }
        
        public void SortByFirstName()
        {
            EmployeeList = EmployeeList.OrderBy(e => e.FirstName).ToList();
        }

        public void SortByLastName()
        {
            EmployeeList = EmployeeList.OrderBy(e => e.LastName).ToList();
        }

        public void SortBySalary()
        {
            EmployeeList = EmployeeList.OrderBy(e => e.Salary).ToList();
        }
        
        public void SearchEmployeeByKeyword(string keyword)
        {
            var employee = EmployeeList.FirstOrDefault(e => 
                e.FirstName.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.LastName.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.AccountNumber.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.Department.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.Position.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase)
            );

            if (employee != null)
            {
                Console.WriteLine(employee.Info());
            }
            else
            {
                Console.WriteLine("No employee found with the given keyword.");
            }
        }
        
        public void AdvancedSearchEmployee(string query)
        {
            var searchTerms = query.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var employee = EmployeeList.FirstOrDefault(e =>
                searchTerms.Any(term =>
                    e.FirstName.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    e.LastName.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    e.AccountNumber.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    e.Department.Name.Contains(term, StringComparison.OrdinalIgnoreCase) ||
                    e.Position.Name.Contains(term, StringComparison.OrdinalIgnoreCase)
                )
            );

            if (employee != null)
            {
                Console.WriteLine(employee.Info());
            }
            else
            {
                Console.WriteLine("No employee found with the given search terms.");
            }
        }
    }

    public class DepartmentService
    {
        public List<Department> DepartmentList { get; set; }

        public DepartmentService()
        {
            DepartmentList = new List<Department>();
        }

        public void AddDepartment(int id, string name)
        {
            Department department = new Department(id, name);
            DepartmentList.Add(department);
        }

        public void DepartmentInfo(int departmentId)
        {
            Department department = DepartmentList.FirstOrDefault(d => d.Id == departmentId);
            
            if (department != null)
            {
                Console.WriteLine(department.Info());
            }
            else
            {
                Console.WriteLine("Department does not exist.");
            }
        }

        public void UpdateDepartmentInfo(int departmentId, int newId, string name)
        {
            Department department = DepartmentList.FirstOrDefault(d => d.Id == departmentId);

            if (department != null)
            {
                department.ChangeData(newId, name);
            }
            else
            {
                Console.WriteLine("Department does not exist.");
            }
        }

        public void DepartmentEmployees(int departmentId)
        {
            Department department = DepartmentList.FirstOrDefault(d => d.Id == departmentId);

            if (department != null)
            {
                Console.WriteLine("Employees in department:");
                foreach (var employee in department.Employees)
                {
                    Console.WriteLine(employee.Info());
                }
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Department Does not exist.");
            }
        }

        public Department AddEmployeeTo(string departmentName)
        {
            Department department = DepartmentList.FirstOrDefault(d => d.Name == departmentName);

            if (department != null)
            {
                return department;
            }
            else
            {
                return null;
            }
        }

        public void SortDepartmentEmployeesByPosition(int departmentId)
        {
            Department department = DepartmentList.FirstOrDefault(d => d.Id == departmentId);

            department.Employees = department.Employees.OrderBy(e => e.Position.Name).ToList();
        }
        
        public void SortDepartmentEmployeesByProjectValues(int departmentId)
        {
            Department department = DepartmentList.FirstOrDefault(d => d.Id == departmentId);

            if (department != null)
            {
                department.Employees = department.Employees
                    .OrderByDescending(e => e.Projects.Sum(p => p.Value))
                    .ToList();
            }
            else
            {
                Console.WriteLine("Department does not exist.");
            }
        }
    }

    public class PositionService
    {
        public List<Position> PositionList { get; set; }
        
        public PositionService()
        {
            PositionList = new List<Position>();
        }

        public void AddPosition(int id, string name)
        {
            Position position = new Position(id, name);
            PositionList.Add(position);
        }

        public void UpdatePositionInfo(int positionId, int newId, string name)
        {
            Position position = PositionList.FirstOrDefault(p => p.Id == positionId);

            if (position != null)
            {
                position.ChangeData(newId, name);
            }
            else
            {
                Console.WriteLine("Position does not exist.");
            }
        }

        public Position AssignEmployeeTo(string positionName)
        {
            Position position = PositionList.FirstOrDefault(p => p.Name == positionName);

            if (position != null)
            {
                return position;
            }
            else
            {
                return null;
            }
        }

        public void Top5Positions()
        {
            // Створення словника для зберігання рейтингу по кожній посаді
            Dictionary<Position, decimal> positionRatings = new Dictionary<Position, decimal>();

            // Заповнення словника рейтингами для кожної посади
            foreach (var position in PositionList)
            {
                if (position.PositionEmployees.Count > 0)
                {
                    decimal totalRating = position.PositionEmployees.Sum(e => e.Salary / e.WorkingHours);
                    decimal averageRating = totalRating / position.PositionEmployees.Count;
                    positionRatings[position] = averageRating;
                }
            }

            // Відсортувати посади за рейтингом у порядку спадання і взяти топ 5
            var top5positions = positionRatings.OrderByDescending(p => p.Value).Take(5);

            // Вивести топ 5 посад
            Console.WriteLine("Top 5 prestigious positions based on Salary/WorkingHours:");
            foreach (var position in top5positions)
            {
                Console.WriteLine($"{position.Key.Name} with average rating: {position.Value}");
            }
        }
        
        public void MostProfitableEmployee(int positionId)
        {
            Position position = PositionList.FirstOrDefault(p => p.Id == positionId);
            
            if (position == null || position.PositionEmployees == null || position.PositionEmployees.Count == 0)
            {
                Console.WriteLine("No employees found for the given position.");
                return;
            }

            Employee mostProfitableEmployee = null;
            decimal highestProfitabilityRatio = 0;

            foreach (var employee in position.PositionEmployees)
            {
                if (employee.WorkExperience == 0) continue;

                decimal totalProjectValue = employee.Projects.Sum(p => p.Value);
                decimal profitabilityRatio = totalProjectValue / employee.WorkExperience;

                if (mostProfitableEmployee == null || profitabilityRatio > highestProfitabilityRatio)
                {
                    mostProfitableEmployee = employee;
                    highestProfitabilityRatio = profitabilityRatio;
                }
            }
            
            Console.WriteLine($"Most profitable employee for position {position.Name}: {mostProfitableEmployee.FirstName} {mostProfitableEmployee.LastName} with profitability ratio: {highestProfitabilityRatio}");
        }
    }

    // Тільки для збереження проєктів
    public class ProjectService
    {
        public List<Project> Projects { get; set; }

        public ProjectService()
        {
            Projects = new List<Project>();
        }

        public void AddProject(Project project)
        {
            Projects.Add(project);
        }
        
        public void SearchProjectByKeyword(string keyword)
        {
            var project = Projects.FirstOrDefault(p => 
                p.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                p.Value.ToString().Contains(keyword)
            );

            if (project != null)
            {
                Console.WriteLine($"{project.Name}, Value: {project.Value}");
            }
            else
            {
                Console.WriteLine("No project found with the given keyword.");
            }
        }
    }

    public class SearchService
    {
        private EmployeeService _employeeService;
        private ProjectService _projectService;
        private PositionService _positionService;
        private DepartmentService _departmentService;

        public SearchService(EmployeeService employeeService, ProjectService projectService,
            PositionService positionService, DepartmentService departmentService)
        {
            _employeeService = employeeService;
            _projectService = projectService;
            _positionService = positionService;
            _departmentService = departmentService;
        }

        public void SearchAllByKeyword(string keyword)
        {
            var employee = _employeeService.EmployeeList.FirstOrDefault(e =>
                e.FirstName.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.LastName.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.AccountNumber.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.Department.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                e.Position.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase)
            );

            var project = _projectService.Projects.FirstOrDefault(p =>
                p.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase) ||
                p.Value.ToString().Contains(keyword)
            );

            var position = _positionService.PositionList.FirstOrDefault(p =>
                p.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase)
            );

            var department = _departmentService.DepartmentList.FirstOrDefault(d =>
                d.Name.Contains(keyword, StringComparison.OrdinalIgnoreCase)
            );

            Console.WriteLine($"Search results for keyword: {keyword}");

            if (employee != null)
            {
                Console.WriteLine("\nEmployee:");
                Console.WriteLine(employee.Info());
            }
            else
            {
                Console.WriteLine("No employee found with the given keyword.");
            }

            if (project != null)
            {
                Console.WriteLine("\nProject:");
                Console.WriteLine($"{project.Name}, Value: {project.Value}");
            }
            else
            {
                Console.WriteLine("No project found with the given keyword.");
            }

            if (position != null)
            {
                Console.WriteLine("\nPosition:");
                Console.WriteLine(position.Info());
            }
            else
            {
                Console.WriteLine("No position found with the given keyword.");
            }

            if (department != null)
            {
                Console.WriteLine("\nDepartment:");
                Console.WriteLine(department.Info());
            }
            else
            {
                Console.WriteLine("No department found with the given keyword.");
            }
        }
    }
}
